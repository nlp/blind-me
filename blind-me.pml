<?xml version="1.0" encoding="UTF-8" ?>
<Package name="blind-me" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="plugin" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="solitary" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Nao/Sit/blinded_1" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Nao/Sit/blinded_2" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Nao/Sit/blinded_3" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Nao/SitOnPod/blinded_2" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Nao/SitOnPod/blinded_3" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Nao/SitOnPod/blinded_1" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Nao/Stand/blinded_1" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Nao/Stand/blinded_2" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Nao/Stand/blinded_3" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Others/Stand/blinded_1" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Others/Stand/blinded_2" xar="behavior.xar" />
	<BehaviorDescription name="behavior" src="animations/Others/Stand/blinded_3" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
	<Dialog name="collaborative-blind-me" src="dialog/collaborative-blind-me/collaborative-blind-me.dlg"/>
	<Dialog name="solitary-blind-me" src="dialog/solitary-blind-me/solitary-blind-me.dlg"/>
    </Dialogs>
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="autonomousLaunchpadPlugin.xml" src="plugin/autonomousLaunchpadPlugin.xml" />
    </Resources>
    <Topics>
        <Topic name="collaborative-blind-me_enu" src="dialog/collaborative-blind-me/collaborative-blind-me_enu.top"  topicName="collaborative-blind-me" language="en_US"/>
        <Topic name="collaborative-blind-me_frf" src="dialog/collaborative-blind-me/collaborative-blind-me_frf.top"  topicName="collaborative-blind-me" language="fr_FR"/>
        <Topic name="collaborative-blind-me_jpj" src="dialog/collaborative-blind-me/collaborative-blind-me_jpj.top"  topicName="collaborative-blind-me" language="ja_JP"/>
        <Topic name="collaborative-blind-me_czc" src="dialog/collaborative-blind-me/collaborative-blind-me_czc.top"  topicName="collaborative-blind-me" language="cs_CZ"/>
        <Topic name="solitary-blind-me_enu" src="dialog/solitary-blind-me/solitary-blind-me_enu.top"  topicName="solitary-blind-me" language="en_US"/>
        <Topic name="solitary-blind-me_frf" src="dialog/solitary-blind-me/solitary-blind-me_frf.top"  topicName="solitary-blind-me" language="fr_FR"/>
        <Topic name="solitary-blind-me_jpj" src="dialog/solitary-blind-me/solitary-blind-me_jpj.top"  topicName="solitary-blind-me" language="ja_JP"/>
        <Topic name="solitary-blind-me_czc" src="dialog/solitary-blind-me/solitary-blind-me_czc.top"  topicName="solitary-blind-me" language="cs_CZ"/>
    </Topics>
    <IgnoredPaths>
        <Path src=".metadata" />
    </IgnoredPaths>
</Package>
