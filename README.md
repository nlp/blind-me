# Blind me - Czech dialogs

Developed at [NLP Centre](https://nlp.fi.muni.cz/en), [FI MU](https://www.fi.muni.cz/index.html.en) for [Karel Pepper](https://nlp.fi.muni.cz/trac/pepper)

This is just an extension of original `blind-me` application
from Softbank Robotics.

## Installation

* you need to have the `blind-me` application installed
* copy `/home/nao/.local/share/PackageManager/apps/blind-me` to
  new place
* copy files from this project over the files from `blind-me`
* [make and install](https://nlp.fi.muni.cz/trac/pepper/wiki/InstallPkg) the new package over the previous version

